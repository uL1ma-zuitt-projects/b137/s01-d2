package com.zuitt.s01d2;

public class DataTypesAndVariables {
    public static void main(String[] args){
        // Variable declaration with initialization
        int firstNumber = 0;

        // Variable declaration without initialization
        int secondNumber;

        secondNumber = 23; // Initialization after declaration

        //Concatenation
        System.out.println("The value of the first number: " + firstNumber);
        System.out.println("The value of the second number: " + secondNumber);

        //Mini-activity:

        int age = 24;
        int weight = 78;
        char gender = 'M';
        boolean isSingle = true;

        System.out.println("Age is: " + age);
        System.out.println("Weight is: " + weight);
        System.out.println("Gender is: " + gender);
        System.out.println("Is single: " + isSingle);

        // Non-Primitive Data types:

        // Create a string variable for a name
        // <data_type> <identifier> = "value";

        String name = "Juan Dela Cruz";
        String concatenatedName = "Juan" + "Dela" + "Cruz";

        System.out.println("name: " + name);
        System.out.println("concatenatedName: " + concatenatedName);
    }
}
